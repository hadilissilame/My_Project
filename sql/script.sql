--  -----------------------------------------
--  BDD FormationPlus
--  -----------------------------------------

DROP DATABASE FormationPlus;

CREATE DATABASE FormationPlus;

USE FormationPlus;

--  -----------------------------------------
--  Créations des tables de la BDD 
--  -----------------------------------------

CREATE TABLE IF NOT EXISTS Convention (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(255) NOT NULL,
	nbHeur NUMERIC(7, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Etudiant (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(255) NOT NULL,
	prenom VARCHAR(255) NOT NULL,
	mail VARCHAR(255) NOT NULL,
    id_convention INT NOT NULL
);

CREATE TABLE IF NOT EXISTS Attestation (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	id_etudiant INT NOT NULL,
	id_convention INT NOT NULL,
    message TEXT
);

--  -----------------------------------------
--  Insertion des données dans chaque table
--  -----------------------------------------

INSERT INTO Convention VALUES
(NULL	, 	"Convention_Formation_Apprentissage",	35),
(NULL	,	"Convention_Formation_Continue"		, 	39),
(NULL	, 	"Convention_Formation_Initiale"		, 	33),
(NULL	,	"Convention_De_Stage"				, 	31);

INSERT INTO Etudiant VALUES
(NULL	,	"CRESSON"	,	"Anaïs"		,	"anaïs.cresson@gmail.com"		,	1),
(NULL	,	"GAVICONNI"	, 	"Bastien"	, 	"bastien.gaviconni@gmail.com"	, 	1),
(NULL	,	"MORGANE"	, 	"Clara"		, 	"clara.morgane@gmail.com"		, 	2),
(NULL	,	"DJEBBARI"	, 	"Dounia"	, 	"dounia.djebbari@gmail.com"		, 	2),
(NULL	,	"FORSEID"	, 	"Ethan"		, 	"ethan.forseid@gmail.com"		, 	2),
(NULL	,	"GOULAME"	, 	"Fouad"		, 	"fouad.goulame@gmail.com"		, 	3),
(NULL	,	"SOLANGE"	, 	"Gwen"		, 	"gwen.solange@gmail.com"		, 	3),
(NULL	,	"ISSILAME"	, 	"Hadil"		, 	"hadil.issilame@gmail.com"		, 	3),
(NULL	,	"ABDALLAH"	, 	"Imane"		, 	"imane.abdallah@gmail.com"		, 	3),
(NULL	,	"GRAVELIN"	, 	"Julien"	, 	"julien.gravelin@gmail.com"		, 	4),
(NULL	,	"BENARZID"	, 	"Karim"		, 	"karim.benarzid@gmail.com"		, 	4),
(NULL	,	"DELAMARRE"	, 	"Lucie"		, 	"lucie.delamarre@gmail.com"		, 	4);

INSERT INTO Attestation VALUES
(NULL	,	1	,	4	,	""),
(NULL	,	2	, 	4	, 	""),
(NULL	,	3	, 	4	, 	""),
(NULL	,	4	, 	1	, 	""),
(NULL	,	5	, 	1	, 	""),
(NULL	,	6	, 	1	, 	""),
(NULL	,	7	, 	1	, 	""),
(NULL	,	8	, 	2	, 	""),
(NULL	,	9	, 	2	, 	""),
(NULL	,	10	, 	2	, 	""),
(NULL	,	11	, 	3	, 	""),
(NULL	,	12	, 	3	, 	"");