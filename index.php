<?php
    include "include/database.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>MyProject</title>
</head>
<body>

    <?php

        if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['nbHeur'])) 
        {
            $sql = 'SELECT Etudiant.nom, Etudiant.prenom, Convention.nbHeur FROM Etudiant INNER JOIN Convention ON Etudiant.id_convention = Convention.id';

            $req = mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());

            $data = mysql_fetch_array($req);

            mysql_free_result($req);

            mysql_close();

                echo 'Nous venons d\'insérer un nouveau disque : '.$_POST['nom'].' de '.$_POST['prenom'].' appartenant au '.$_POST['nbHeur'];
        }
        
    ?>

    <header class="example">
        <img src="img/formation.jpeg">
    </header>

    <div class="content">
        <form action="formulaire.php" method="post">
            <fieldset>
                <legend>Informations Personnelles</legend>
                <label for="prenom">Liste des étudiants</label><br>
                <select name="nom" id="prenom">
                    <option value="">--Faites votre choix--</option>
                    <option value="Anaïs">Anaïs</option>
                    <option value="Bastien">Bastien</option>
                    <option value="Clara">Clara</option>
                    <option value="Dounia">Dounia</option>
                    <option value="Ethan">Ethan</option>
                    <option value="Fouad">Fouad</option>
                    <option value="Gwen">Gwen</option>
                    <option value="Hortence">Hortence</option>
                    <option value="Imane">Imane</option>
                    <option value="Julien">Julien</option>
                    <option value="Karim">Karim</option>
                    <option value="Lucie">Lucie</option>
                </select>
    
                <label for="nom">Convention</label><br>
                <input type="text" name="nom" id="nom" disabled="disabled" />
            </fieldset>
            <fieldset>
                <legend>Message</legend>
                <label for="message">Contenu</label>
                <textarea name="message" id="message" cols="30" rows="10" placeholder="Votre Message">
                    <value>Bonjour <?php echo $_POST['nom']; ?> <?php echo $_POST['prenom']; ?>,</value><br><br>
                    <value>Vous avez suivi <?php echo $_POST['nbHeur']; ?> de formation chez FormationPlus.</value><br>
                    <value>Pouvez-vous nous retourner ce mail avec la pièce jointe signée.<value><br><br>
                    <value>Cordialement,</value><br>
                    <value>FormationPlus</value>
                </textarea>
            </fieldset>
            <input type="submit" value="Envoyer">        
        </form>
    </div>    
</body>
</html>